'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("auth_videos", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      users_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "users", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",

      },
      kids_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "kids", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      videos_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "videos", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("auth_videos");
  }
};
