'use strict';


module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("kids", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      users_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "users", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",

      },
      nickname: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      age: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      avatar: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: { model: "avatars", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      style: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: { model: "styles", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("kids");
  }
};

