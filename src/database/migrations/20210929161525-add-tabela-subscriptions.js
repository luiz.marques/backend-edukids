'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("subscriptions", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "users", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      plans_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: "plans", key: "id" },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      active: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      expires_in: {
        allowNull: false,
        type: Sequelize.DATEONLY,
      },
    });

  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("subscriptions");
  }
};
