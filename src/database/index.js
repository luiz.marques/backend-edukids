const Sequelize = require("sequelize");
const dbConfig = require("../config/database");

const Video = require("./models/Video");
const Categories = require("./models/Categories");
const User = require("./models/User");
const Kids = require("./models/Kids");
const AuthVideo = require("./models/AuthVideo");
const Plan = require("./models/Plan");
const Subscription = require("./models/Subscription");
const Avatar = require("./models/Avatar");

const connection = new Sequelize(dbConfig["development"]);


Video.init(connection);
Categories.init(connection);
User.init(connection);
AuthVideo.init(connection);
Plan.init(connection);
Subscription.init(connection);
Avatar.init(connection);
Kids.init(connection);

// associaçoes 
User.associate(connection.models);
Video.associate(connection.models);
AuthVideo.associate(connection.models);
Kids.associate(connection.models);


module.exports = connection;
