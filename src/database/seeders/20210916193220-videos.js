'use strict';

module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('videos', [
    {
      title: 'Vídeo educativo Infantil - Farm Animal',
      video_url: 'https://www.youtube.com/watch?v=z8txiWkNzLk',
      thumb_url: 'https://i.ytimg.com/an_webp/z8txiWkNzLk/mqdefault_6s.webp',
      description: 'SONS DOS ANIMAIS COM DESENHOS',
      pg: 1,
      category1: 3,
      // category2:
      // category3: 
      language: 2,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Números',
      video_url: 'https://www.youtube.com/watch?v=H66tTFTN3ug',
      thumb_url: 'https://i.ytimg.com/an_webp/H66tTFTN3ug/mqdefault_6s.webp?du=3000&sqp=CMCykooG&rs=AOn4CLDDQnGe8B3eVnbnBkcqcWYVhE4-lA',
      description: 'Ensinando os números para crianças',
      pg: 1,
      category1: 7,
      // category2:
      // category3: 
      language: 5,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Rimando com os bebês - Five Little Babies ',
      video_url: 'https://www.youtube.com/watch?v=6tzG_-XHUBQ',
      thumb_url: 'https://i.ytimg.com/an_webp/v3Mr1XqTrlk/mqdefault_6s.webp?du=3000&sqp=CPyZkooG&rs=AOn4CLDLpAnHsRKKGo3YPbhzbQFoHgDARg',
      description: 'RIMA COM OS BEBÊS',
      pg: 1,
      category1: 5,
      //    category2: '',
      //    category3: '',
      language: 2,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Our Favorite Numbers Songs',
      video_url: 'https://www.youtube.com/watch?v=V_lgJgBbqWE',
      thumb_url: 'https://i.ytimg.com/an_webp/V_lgJgBbqWE/mqdefault_6s.webp?du=3000&sqp=CKeykooG&rs=AOn4CLCVvDAzqsGIr1gFS_u0e159HOcEdQ',
      description: 'Learn how to count with this collection of numbers songs for kids from Super Simple Songs! ',
      pg: 1,
      category1: 8,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Canção do banho em desenho para bebês - Little Baby Bum',
      video_url: 'https://www.youtube.com/watch?v=RmJpsqVwAno',
      thumb_url: 'https://i.ytimg.com/an_webp/RmJpsqVwAno/mqdefault_6s.webp?du=3000&sqp=CPKkkooG&rs=AOn4CLBnlkRLVxwwW7DK0GJBDrqNtUIHpg',
      description: 'CANÇÃO PARA A HORA DO BANHO',
      pg: 1,
      category1: 7,
      //      category2: '',
      //    category3: '',
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'A E I O U',
      video_url: 'https://www.youtube.com/watch?v=VbvVC8_Rp54',
      thumb_url: 'https://i.ytimg.com/an_webp/VbvVC8_Rp54/mqdefault_6s.webp?du=3000&sqp=COC-kooG&rs=AOn4CLCSuPBSgPMp3pxNy59CnSZn7KsFjA',
      description: 'Se o que você procura para as crianças é: música infantil, vídeos educativos, história infantil e diversão, você está no lugar certo! Inscreva-se no canal e não perca nenhuma novidade!',
      pg: 1,
      category1: 3,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'FUNK Infantil',
      video_url: 'https://www.youtube.com/watch?v=uMBKXONjGnU',
      thumb_url: 'https://i.ytimg.com/an_webp/uMBKXONjGnU/mqdefault_6s.webp?du=3000&sqp=CLzKkooG&rs=AOn4CLDCdD2I142Hx12prKheqLLTkeYpcA',
      description: 'Remix dos melhores funk infantil para festa de criança 2020',
      pg: 1,
      category1: 6,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Aprendendo o pezinho com o bebê - Canal do Joãozinho',
      video_url: 'https://www.youtube.com/watch?v=2OwSiBAaZPM',
      thumb_url: 'https://i.ytimg.com/an_webp/2OwSiBAaZPM/mqdefault_6s.webp?du=3000&sqp=CKbEkooG&rs=AOn4CLCC09CmsqgHXrr7TwdAwSovPdm9Kg',
      description: 'PARTE DO CORPO PARA BEBÊS',
      pg: 1,
      category1: 10,
      //category2: '',
      //category3: '',
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Vamos à Floresta',
      video_url: 'https://www.youtube.com/watch?v=YeLsFw4Re3o',
      thumb_url: 'https://i.ytimg.com/an_webp/YeLsFw4Re3o/mqdefault_6s.webp?du=3000&sqp=CMjBkooG&rs=AOn4CLBGp_c5sydLJ2o3QCJ62nFHHc3i-A',
      description: 'Vamos Todos À Floresta | Canções infantis em português | ChuChu TV Coleção',
      pg: 1,
      category1: 11,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Formas e Cores',
      video_url: 'https://www.youtube.com/watch?v=Y5aXav32xCQ',
      thumb_url: 'https://i.ytimg.com/an_webp/Y5aXav32xCQ/mqdefault_6s.webp?du=3000&sqp=CNjYkooG&rs=AOn4CLBYBmQu9VQxq-g3DRGrzArMxUgttw',
      description: 'Shapes Família Finger, popular viveiro rima para as crianças a aprender formas. Então, se divertir aprendendo em "português".',
      pg: 1,
      category1: 1,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Canção de ninar do Brahms - Little Baby Bum',
      video_url: 'https://www.youtube.com/watch?v=BSgBzPXC3LQ',
      thumb_url: 'https://i.ytimg.com/an_webp/BSgBzPXC3LQ/mqdefault_6s.webp?du=3000&sqp=CPTUkooG&rs=AOn4CLDeOtrTGy2aGJoBfwCht4_NyDumEw',
      description: 'MUSICAS PARA O BEBÊ DORMIR',
      pg: 1,
      category1: 1,
      //category2: '',
      //category3: '',
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Colors with Surprise',
      video_url: 'https://www.youtube.com/watch?v=9_WBQISVHnw',
      thumb_url: 'https://i.ytimg.com/an_webp/9_WBQISVHnw/mqdefault_6s.webp?du=3000&sqp=COyykooG&rs=AOn4CLCGhqF4mvC9K2-DgkW17glDHAOAvw',
      description: 'Best Learning Video for Toddlers Learn Colors with Crayon Surprises! In this preschool learning video for kids, teach kids colors, common words, and simple sentences with this educational video for toddlers and babies. We ll open each of our rainbow colored crayons to find three toy ',
      pg: 1,
      category1: 1,
      // category2:
      // category3: 
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      title: 'Suco de Fruta Em Uma Garrafa - Família de Gatos',
      video_url: 'https://www.youtube.com/watch?v=aibBjfPQT1Q',
      thumb_url: 'https://i.ytimg.com/an_webp/aibBjfPQT1Q/mqdefault_6s.webp?du=3000&sqp=CMSskooG&rs=AOn4CLCqApxHWAjjbtk2XWi_7yeQCQpsbw',
      description: 'FAMÍLIA DE GATOS UNIDOS PARA FAZER SUCO',
      pg: 1,
      category1: 1,
      // category2: '',
      // category3: '',
      language: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
  ], {}),

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('videos', {
      [Op.or]: [
        { title: '' },
      ]
    });
  }
};