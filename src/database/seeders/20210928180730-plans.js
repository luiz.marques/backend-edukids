'use strict';

module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('plans', [
    { 
      name: 'Platinum',
      term: 'Semestral',
      price: 19.99,
      concurrent: 4,
    },
    { 
      name: 'Gold',
      term: 'Trimestral',
      price: 14.99,
      concurrent: 2,
    },
    { 
      name: 'Silver',
      term: 'Mensal',
      price: 9.99,
      concurrent: 1,
    },
    
  ], {}),

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('plans', {
      [Op.or]: [
        { name: 'Platinum' },
        { name: 'Gold' },
        { name: 'Silver' },
        
      ]
    });
  }
};
