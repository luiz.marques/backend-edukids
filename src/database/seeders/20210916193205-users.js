'use strict';

module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('users', [
    { 
      name: 'Luiz Fernando Cavalcanti de Mello Marques' ,
      cpf: '108-254-177-08',
      birthday: '1984/02/11',
      email: 'luiz@alak@hotmail.com',
      phone: '21992044359',
      address: 'Rua Conde de Lages, 54, apto 112 bloco B',
      password: '12345asdfge',
      user_type: 0,
      created_at: new Date(),
      updated_at: new Date()
    },
  ], {}),

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('users', {
      [Op.or]: [
        { name: 'Luiz Fernando Cavalcanti de Mello Marques' },
      ]
    });
  }
};
