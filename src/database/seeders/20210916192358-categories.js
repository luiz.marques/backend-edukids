'use strict';

module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('categories', [
    { category: 'Músicais' },
    { category: 'Desenhos' },
    { category: 'Cores' },
    { category: 'Animais' },
    { category: 'Aprendizagem' },
    { category: 'Contas' },
    { category: 'Letras' },
    { category: 'Experiências' },
    { category: 'Idiomas' },
    { category: 'Historias' },
    { category: 'Animes' },
    { category: 'Musicas' },
    { category: 'Clipes' },
    { category: 'inclusão' },
  ], {}),

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('categories', {
      [Op.or]: [
        { category: 'Músicais' },
        { category: 'Desenhos' },
        { category: 'Cores' },
        { category: 'Animais' },
        { category: 'Aprendizagem' },
        { category: 'Contas' },
        { category: 'Letras' },
        { category: 'Experiências' },
        { category: 'Idiomas' },
        { category: 'Historias' },
        { category: 'Animes' },
        { category: 'Musicas' },
        { category: 'Clipes' },
        { category: 'inclusão' },
      ]
    });
  }
};
