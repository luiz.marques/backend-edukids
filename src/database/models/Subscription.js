const { Model, DataTypes } = require("sequelize");

class Subscription extends Model {
  static init(sequelize) {
    super.init(
      {
        user_id: DataTypes.INTEGER,
        plans_id: DataTypes.INTEGER,
        active: DataTypes.BOOLEAN,
        expires_in: DataTypes.DATEONLY
      },
      {
        sequelize,
        tableName: "subscriptions",
        createdAt: false,
        updatedAt: false,
      }
    );
  }

}

module.exports = Subscription;