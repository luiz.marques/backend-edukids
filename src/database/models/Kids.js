const { Model, DataTypes } = require("sequelize");
const Avatar = require('./Avatar')

class Kids extends Model {
  static init(sequelize) {
    super.init(
      {
        users_id: DataTypes.INTEGER,
        nickname: DataTypes.STRING,
        age: DataTypes.INTEGER,
        avatar_id: {
          type: DataTypes.INTEGER,
          references: {
            model: Avatar,
            key: 'id'
          }
      
        },
        style: DataTypes.INTEGER,
      },
      {
        sequelize,
        tableName: "kids",
        createdAt: false,
        updatedAt: false,
      }
    );
  }
  static associate(models) {
    this.belongsTo(models.Avatar);
  }
}

module.exports = Kids;