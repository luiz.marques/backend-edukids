const { Model, DataTypes } = require("sequelize");

class Categories extends Model {
  static init(sequelize) {
    super.init(
      {
        category: DataTypes.STRING,
      },
      {
        sequelize,
        tableName:  "categories",
        createdAt: false,
        updatedAt: false,
      }
    );
  }
}

module.exports = Categories;