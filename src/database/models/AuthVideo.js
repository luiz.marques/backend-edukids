const { Model, DataTypes } = require("sequelize");

class AuthVideo extends Model {
  static init(sequelize) {
    super.init(
      {
        users_id: DataTypes.INTEGER,
        kids_id: DataTypes.INTEGER,
        videos_id: DataTypes.INTEGER
      },
      {
        sequelize,
        tableName: "auth_videos",
        createdAt: false,
        updatedAt: false,
      }
    );
  }

  //Esse código vincula a tabela Auth_Videos à tabela Video. Como já são relacionadas diretamente no BD, não usa o through na sintaxe. 
  static associate(models) {
    this.belongsTo(models.Video, {
      foreignKey: "videos_id",
      as: "videos",
    });
  }
}


module.exports = AuthVideo;