const { Model, DataTypes } = require("sequelize");

class Plan extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        term: DataTypes.STRING,
        price: DataTypes.DECIMAL,
        concurrent: DataTypes.INTEGER
      },
      {
        sequelize,
        tableName: "plans",
        createdAt: false,
        updatedAt: false,
      }
    );
  }

}

module.exports = Plan;
