const { Model, DataTypes, ForeignKeyConstraintError } = require("sequelize");

class Avatar extends Model {
  static init(sequelize) {
    super.init(
      {
        avatar_url: DataTypes.TEXT,
      },
      {
        sequelize,
        tableName: "avatars",
        createdAt: false,
        updatedAt: false,
      }
    );
  }
  static associate(models) {
    this.hasMany(models.Kids,
      {
        foreignKey: "avatar_id",
      });
  }
}

module.exports = Avatar;