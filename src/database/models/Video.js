const { Model, DataTypes } = require("sequelize");

class Video extends Model {
  static init(sequelize) {
    super.init(
      {
        title: DataTypes.STRING,
        video_url: DataTypes.TEXT,
        thumb_url: DataTypes.TEXT,
        description: DataTypes.TEXT,
        pg: DataTypes.INTEGER,
        language: DataTypes.INTEGER,
        category1: DataTypes.INTEGER,
        category2: DataTypes.INTEGER,
        category3: DataTypes.INTEGER,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        sequelize,
        tableName: "videos",
        createdAt: false,
        updatedAt: false,
      }
    );
  }
  static associate(models) {
    this.belongsToMany(models.User, {
      foreignKey: "videos_id",
      through: "auth_videos",
      as: "users",
    });
    this.belongsTo(models.Categories, {
      foreignKey: "category1",
      allowNull: true,
      as: "categories1"
    });
    this.belongsTo(models.Categories, {
      foreignKey: "category2",
      allowNull: true,
      as: "categories2"
    });
    this.belongsTo(models.Categories, {
      foreignKey: "category3",
      allowNull: true,
      as: "categories3"
    });
  }
  
  
}

module.exports = Video;
