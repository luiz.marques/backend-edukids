const { Model, DataTypes } = require("sequelize");

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        cpf: DataTypes.STRING,
        birthday: DataTypes.DATEONLY,
        email: DataTypes.STRING,
        phone: DataTypes.STRING,
        address: DataTypes.STRING,
        password: DataTypes.STRING,
        user_type: DataTypes.INTEGER,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        sequelize,
        tableName: "users",
        createdAt: false,
        updatedAt: false,
      }
    );
  }
  static associate(models) {
    this.belongsToMany(models.Video, {
      foreignKey: "users_id",
      through: "auth_videos",
      as: "videos",
    });
  }
  static associate(models) {
    this.belongsToMany(models.Plan, {
      foreignKey: "plans_id",
      through: "subscription",
      as: "plans",
    });
  }
}

module.exports = User;