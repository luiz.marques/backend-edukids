require('dotenv').config()
module.exports = {
  development: {
    dialect: process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    define: {
      timestamps: true, //  para incluir o created_at e updated_ad
      underscored: true, // modelo snackcase
    },
  },
  production: {},
};
