const VideoController = require("./../../../controller/VideoController");
const { videoSchema, videoParams } = require("./../../../schemas/videoSchema");

module.exports = (routes) => {
    /**
     * 
     * @swagger
     *   /videos:
     *     get:
     *       description: Lista todos os vídeos
     *       responses: 
     *         200:
     *           description: retorno de sucesso 
     *           schema: 
     *             type: array 
     *             items:
     *                type: object
     *                properties: 
     *                  id: 
     *                    type: integer
     *                    example: 1
     *                  title: 
     *                    type: string
     *                    example: Tubarão 13 13 13
     * 
     */
    routes.get("/public/videos", VideoController.index);
    
    /**
     * 
     * @swagger
     *   /videos/id:
     *     get:
     *       description: Lista todos os vídeos
     *       responses: 
     *         200:
     *           description: retorno de sucesso 
     *           schema: 
     *             type: array 
     *             items:
     *                type: object
     *                properties: 
     *                  id: 
     *                    type: integer
     *                    example: 1
     *                  title: 
     *                    type: string
     *                    example: Tubarão 13 13 13
     * 
     */
  routes.get("/public/videos/:id", [videoParams], VideoController.show);
    
  /**
     * 
     * @swagger
     *   /videos:
     *     post:
     *       description: Lista todos os vídeos
     *       responses: 
     *         200:
     *           description: retorno de sucesso 
     *           schema: 
     *             type: array 
     *             items:
     *                type: object
     *                properties: 
     *                  id: 
     *                    type: integer
     *                    example: 1
     *                  title: 
     *                    type: string
     *                    example: Tubarão 13 13 13
     * 
     */
  routes.post("/public/videos", [videoSchema], VideoController.store);
  
  /**
     * 
     * @swagger
     *   /videos/:id:
     *     put:
     *       description: Lista todos os vídeos
     *       responses: 
     *         200:
     *           description: retorno de sucesso 
     *           schema: 
     *             type: array 
     *             items:
     *                type: object
     *                properties: 
     *                  id: 
     *                    type: integer
     *                    example: 1
     *                  title: 
     *                    type: string
     *                    example: Tubarão 13 13 13
     * 
     */
  routes.put("/public/videos/:id", [videoParams], [videoSchema], VideoController.updateVideo);

  /**
     * 
     * @swagger
     *   /videos/:id:
     *     delete:
     *       description: Lista todos os vídeos
     *       responses: 
     *         200:
     *           description: retorno de sucesso 
     *           schema: 
     *             type: array 
     *             items:
     *                type: object
     *                properties: 
     *                  id: 
     *                    type: integer
     *                    example: 1
     *                  title: 
     *                    type: string
     *                    example: Tubarão 13 13 13
     * 
     */
  routes.delete("/public/videos/:id", [videoParams], VideoController.destroy);
}