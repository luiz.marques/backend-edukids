const VideoController = require("./../../../controller/VideoController");
const { videoSchema, videoParams } = require("./../../../schemas/videoSchema");

module.exports = (routes) => {

   routes.get("/videos", VideoController.index);


   routes.get("/videos/:id", [videoParams], VideoController.show);


   routes.post("/videos", [videoSchema], VideoController.store);


   routes.put("/videos/:id", [videoParams], [videoSchema], VideoController.updateVideo);


   routes.delete("/videos/:id", [videoParams], VideoController.destroy);
}