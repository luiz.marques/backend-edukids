const AuthController = require("./../../../controller/AuthController");

module.exports = (routes) => {
  
     /**
     * 
     * @swagger
     *   /login:
     *     post:
     *       tags: 
     *         - Login
     *       consumes:
     *        - application/json
     *       parameters:
     *         - in: body
     *           name: login
     *           description: Fazer o login.
     *           schema:
     *             type: object
     *             required:
     *               - email
     *               - password
     *             properties:
     *               email:
     *                 type: string
     *                 default: contato@karinepaiva.com
     *               password:
     *                 type: string
     *                 default: 123456
     *       responses: 
     *         200:
     *           description: retorno de sucesso
     *           schema:
     *             type: object
     *             properties: 
     *               tipo: 
     *                 type: string
     *                 example: bearer
     *               token: 
     *                 type: string
     *                 example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzAsIm5hbWUiOiJLYXJpbmUgUm9kcmlndWVzIGRhIFNpbHZhIFBhaXZhIiwiZW1haWwiOiJjb250YXRvQGthcmluZXBhaXZhLmNvbSIsInVzZXJfdHlwZSI6MSwiaWF0IjoxNjMzMDE1ODY4LCJleHAiOjE2MzMwMTk0Njh9.THYevRXZ8wFoaoAK5p2ryvxcUi-V82w2absZEVnxIf
     * 
     */
  routes.post("/login", AuthController.login)
}