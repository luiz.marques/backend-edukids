const SubscriptionController = require("./../../../controller/SubscriptionController");

module.exports = (routes) => {

  routes.get("/subscriptions", SubscriptionController.index);

  routes.get("/subscription/:id", SubscriptionController.show);

  routes.post("/subscription", SubscriptionController.store);

  routes.put("/subscription/:id", SubscriptionController.updateSubscription);

  routes.delete("/subscription", SubscriptionController.destroy);



}