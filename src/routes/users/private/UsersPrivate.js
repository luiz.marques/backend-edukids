const UserController = require("./../../../controller/UserController");
const { userSchema, userSchemaOptional } = require("./../../../schemas/userSchema");

module.exports = (routes) => {

   routes.get("/users", UserController.index);
   
   routes.get("/users/:id", UserController.show);
   
   routes.put("/user/update/:id", UserController.updateUser);

   routes.get("/user/videos", UserController.showAuthVideos);

   routes.get("/user/videos/:id", UserController.showAuthVideos);
   
   routes.get("/user/subscription", UserController.showSubscriptionPlan);


   routes.delete("/users/:id", UserController.destroy);
}


