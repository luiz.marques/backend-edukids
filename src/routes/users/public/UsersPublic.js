const UserController = require("./../../../controller/UserController");
const { userSchema, userSchemaOptional } = require("./../../../schemas/userSchema");

module.exports = (routes) => {
  
   routes.post("/users",  [userSchema], UserController.store);
}


