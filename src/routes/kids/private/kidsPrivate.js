const KidsController = require("../../../controller/KidsController");
const UserController = require("./../../../controller/UserController");

module.exports = (routes) => {
  routes.get("/users/kids", UserController.indexShowKids);

  routes.post("/users/create", KidsController.store);

  routes.put("/users/kids/:id", KidsController.updateKid);

  routes.delete("/users/kids/:id", KidsController.destroy);
}