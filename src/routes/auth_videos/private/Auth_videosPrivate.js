const AuthVideoController = require("./../../../controller/AuthVideoController");
const { videoSchema, videoParams } = require("./../../../schemas/videoSchema");

module.exports = (routes) => {

  routes.get("/auth_videos", AuthVideoController.index);

  routes.get("/auth_videos/:id", AuthVideoController.show);

  routes.post("/auth_videos", AuthVideoController.store);

  routes.delete("/auth_videos", AuthVideoController.destroy);
}