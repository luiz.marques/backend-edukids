const PlansController = require("./../../../controller/PlanController");

module.exports = (routes) => {

  routes.get("/plans", PlansController.index);
  
}