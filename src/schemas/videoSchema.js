const { body, param } = require("express-validator");
const { validateDto } = require("../utils/handler");

exports.videoSchema = validateDto([
  body("title").isString().withMessage("Tipo de dado inválido").notEmpty().withMessage("O título do vídeo é obrigatório"),
  body("video_url").isString().notEmpty().withMessage("A URL do vídeo é obrigatória"),
  body("thumb_url").isString().notEmpty().withMessage("A thumb do vídeo é obrigatória"),
  body("description").isString().notEmpty().withMessage("A descrição do vídeo é obrigatória"),
  body("pg").isNumeric().notEmpty().withMessage("A PG é obrigatória"),
  body("category1").isNumeric().notEmpty().withMessage("A categoria é obrigatória"),
  body("language").isNumeric().notEmpty().withMessage("A descrição do vídeo é obrigatória")
])

exports.videoParams = validateDto([
  param("id").notEmpty().isNumeric().withMessage("Informe um parametro numerico"),
])