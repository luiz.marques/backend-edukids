const { body, param } = require("express-validator");
const { validateDto } = require("../utils/handler");

exports.userSchema = validateDto([
  body("name").isString().notEmpty().withMessage("Usuário é obrigatório"),
  body("cpf").isString().notEmpty().withMessage("CPF é obrigatória"),
  body("birthday").isDate().notEmpty().withMessage("A data é obrigatória"),
  body("email").isEmail().notEmpty().withMessage("Email é obrigatório"),
  body("phone").isString().notEmpty().withMessage("O telefone é obrigatório"),
  body("address").isString().notEmpty().withMessage("O endereço é obrigatório"),
  body("password").isString().notEmpty().withMessage("O password é obrigatório"),
  // body("user_type").isNumeric().notEmpty().withMessage("Tipo de usuário é obrigatório")
])

exports.userSchemaOptional = validateDto([
  body("name").isString().optional({nullable: false}).withMessage("Usuário é obrigatório"),
  body("cpf").isString().optional({nullable: false}).withMessage("CPF é obrigatória"),
  body("birthday").isDate().optional({nullable: false}).withMessage("A data é obrigatória"),
  body("email").isEmail().optional({nullable: false}).withMessage("Email é obrigatório"),
  body("phone").isString().optional({nullable: false}).withMessage("O telefone é obrigatório"),
  body("address").isString().optional({nullable: false}).withMessage("O endereço é obrigatório"),
  body("password").isString().optional({nullable: false}).withMessage("O password é obrigatório"),
  body("user_type").isNumeric().optional({nullable: false}).withMessage("Tipo de usuário é obrigatório")
])