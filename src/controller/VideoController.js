const Video = require("../database/models/Video");
const AuthVideo = require("../database/models/AuthVideo");
const Categories = require("../database/models/Categories");
const { getParentalString } = require("../services/parentalGuide")

module.exports = {
  async index(req, res) {
    const videos = await Video.findAll({
      order: [
        ["id", "ASC"]
      ],
      include: [
        {
          model: Categories,
          as: 'categories1',
          attributes: ["id", "category"],
        },
        {
          model: Categories,
          as: 'categories2',
          attributes: ["id", "category"],
        },
        {
          model: Categories,
          as: 'categories3',
          attributes: ["id", "category"],
        },
      ],
    });
    
    const videosResponse = videos.map(item => {
      return {
        ...item.dataValues,
        parentalGuide: getParentalString(item.dataValues.pg)
      }
    })

    return res.json(videosResponse);
  },

  async show(req, res) {
    const { id } = req.params;

    const video = await Video.findByPk(id, {
      attributes: ["id", "title", "video_url", "thumb_url", "description", "pg", "category1"],
      order: [["id", "desc"]],
      through: {
        attributes: [],
      },
    });
    return res.json(video);
  },

  async showAuthVideos(req, res) {
    const user_id = req.userId
    try {
      const authVideos = await AuthVideo.findAll({
        where: {
          users_id: parseInt(user_id),
          
        },
        attributes: ["id", "users_id", "videos_id"],
        order: [["videos_id", "ASC"]],

        //Para incluir os dados da tabela já vinculada diretamente no BD, basta um include simples do Model (tem que add no require no topo). 
        include: [
          {
            model: Video,
            as: 'videos',
            attributes: ["id", "title", "video_url", "thumb_url", "description", "pg"],
          },
        ],
      });
      return res.json(authVideos)
    } catch (error) {
      console.log(error);
    }
  },

  async store(req, res) {
    const { title, video_url, thumb_url, description, pg, language, category1, category2, category3 } = req.body;
    const created_at = new Date()
    const updated_at = new Date()
    try {
      const video = await Video.create({
        title,
        video_url,
        thumb_url,
        description,
        pg,
        language,
        category1,
        category2,
        category3,
        created_at,
        updated_at
      });
      return res.status(200).json("Video adicionado com sucesso")
    } catch (error) {
      return res.status(400).json("falha ao adicionar o video")
    } {
    }
  },

  async updateVideo(req, res) {
    const { id } = req.params;
    try {
      await Video.update(req.body, {
        where: {
          id: parseInt(id),
        },
      });
      return res.status(200).json("Video atualizado com sucesso")
    } catch (error) {
      return res.status(400).json("falha ao atualizar o video")
    }
  },

  async destroy(req, res) {
    const params = req.params;

    const video = await Video.findOne({
      where: {
        id: parseInt(params.id),
      },
    });

    video.destroy();

    res.json("Video deletado");
  },

};