const Subscriptions = require("../database/models/Subscription");

module.exports = {
  async index(req, res) {
    const subscriptions = await Subscriptions.findAll({
      order: [
        ["id", "ASC"]
      ]
    });

    return res.json(subscriptions);
  },

  async show(req, res) {
    const { id } = req.params;

    const subscription = await Subscriptions.findByPk(Number(id), {
      attributes: ["id", "user_id", "plans_id", "active", "expires_in"],
      order: [["id", "desc"]],
      through: {
        attributes: [],
      },
    });
    return res.json(subscription);
  },

  async store(req, res) {
    const { user_id, plans_id, active, expires_in } = req.body;
    try {
      const subscription = await Subscriptions.create({
        user_id,
        plans_id,
        active,
        expires_in
      });
      return res.status(200).json("Assinatura adicionada com sucesso!")
    } catch (error) {
      return res.status(400).json("Falha ao adicionar a assinatura.")
    } {
    }
  },

  async updateSubscription(req, res) {
    const { id } = req.params;
    try {
      await Subscriptions.update(req.body, {
        where: {
          id: parseInt(id),
        },
      });
      return res.status(200).json("Assinatura atualizada com sucesso!")
    } catch (error) {
      return res.status(400).json("Falha ao atualizar a assinatura.")
    }
  },

  async destroy(req, res) {
    const params = req.params;

    const subscription = await Subscriptions.findOne({
      where: {
        id: parseInt(params.id),
      },
    });

    subscription.destroy();

    res.json("Assinatura cancelada!");
  },

};