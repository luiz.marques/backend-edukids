const Plans = require("../database/models/Plan");

module.exports = {
  async index(req, res) {
    const plans = await Plans.findAll({
      order: [
        ["id", "ASC"]
      ]
    });

    return res.json(plans);
  },

  async show(req, res) {
    const { id } = req.params;

    const plan = await Plans.findByPk(id, {
      attributes: ["id", "name", "term", "price", "concurrent"],
      order: [["id", "desc"]],
      through: {
        attributes: [],
      },
    });
    return res.json(plan);
  },

  async store(req, res) {
    const { id, name, term, price, concurrent } = req.body;
    try {
      const plan = await Plans.create({
        name, 
        term, 
        price, 
        concurrent
      });
      return res.status(200).json("Plano adicionado com sucesso")
    } catch (error) {
      return res.status(400).json("falha ao adicionar o plano")
    } {
    }
  },

  async updatePlan(req, res) {
    const { id } = req.params;
    try {
      await Plans.update(req.body, {
        where: {
          id: parseInt(id),
        },
      });
      return res.status(200).json("Plano atualizado com sucesso")
    } catch (error) {
      return res.status(400).json("falha ao atualizar o plano")
    }
  },

  async destroy(req, res) {
    const params = req.params;

    const plan = await Plans.findOne({
      where: {
        id: parseInt(params.id),
      },
    });

    plan.destroy();

    res.json("Plano deletado");
  },

};