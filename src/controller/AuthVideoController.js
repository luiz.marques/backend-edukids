const AuthVideo = require('../database/models/AuthVideo');
const User = require('../database/models/User');

module.exports = {
  async index(req, res) {
    console.log(req.name, req.userId)
    const authvideo = await AuthVideo.findAll({
      order: [
        ["users_id", "ASC"]
      ]
    });
    return res.json(authvideo);
  },

  async show(req, res) {
    const { id } = req.params
    try {
      const authvideos = await User.findByPk(id, {
        include: [
          {
            association: "videos",
            attributes: ["id", "title", "video_url", "thumb_url", "description", "pg", "category1", "category2", "category3", "language"],
            // order: [["name", "desc"]],
            through: {
              attributes: [],
            },
          },
        ],
      });
      return res.json(authvideos)
    } catch (error) {
      console.log(error);

    }
  },

  async store(req, res) {
    const users_id = req.userId
    const { kids_id, videos_id } = req.body
    try {
      const create = await AuthVideo.create(
        {
          users_id,
          kids_id,
          videos_id
        }
      )
      return res.status(200).json("Inscricao feita com sucesso")
    } catch (error) {
      res.status(400).json("Erro ao processar inscricao")
    }
  },

  async updateAuthVideo(req, res) {
    const { id } = req.params;
    try {
      await AuthVideo.update(req.body, {
        where: {
          id: parseInt(id),
        },
      });
      return res.status(200).json("Video autorizado com sucesso")
    } catch (error) {
      return res.status(400).json("falha ao autorizar o video")
    }
  },

  async destroy(req, res) {
    const { users_id, videos_id } = req.body;
    try {
      const authvideo = await AuthVideo.findOne({
        where: {
          users_id: parseInt(users_id),
          videos_id: parseInt(videos_id)
        },
      });
      authvideo.destroy();
      return res.status(200).json("Auth. vídeos excluído com sucesso!")
    } catch (error) {
      res.status(400).json("Erro ao processar exclusão.")
    }
  },
}

