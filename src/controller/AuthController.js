const Usuario = require("../database/models/User");
const bcrypt = require('bcrypt');
const { generateHash, generateToken } = require("../utils/helper");

module.exports = {
  async login(req, res) {
    const { email, password } = req.body
    const user = await Usuario.findOne({
      where: {
        email
      }
    })
    if (!user) {
      return res.status(400).json({ error: "Usuario não existe!" })
    }
    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({ error: "Senha incorreta!"})
    }
    // if (!user.status) {
    //     return res.status(400).json({ error: "Acesso negado!" })
    // }
    const payloadToken = {
      id: user.id,
      name: user.name,
      email: user.email,
      user_type: user.user_type
    }
    const token = generateToken(payloadToken)
    return res.json({ ...token })
  }
}