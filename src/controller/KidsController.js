const Kids = require("../database/models/Kids");
const { userId } = require('../utils/verifyJWT');

module.exports = {

  async store(req, res) {
    const { nickname, age } = req.body;
    const userId = req.userId;

    try {
      const kid = await Kids.create({
        users_id: userId,
        nickname,
        age,
        avatar_id: 1,
        style: 1
      });
      return res.status(200).json("Perfil adicionado com sucesso!")
    } catch (error) {
      return res.status(400).json("Falha ao adicionar o perfil.")
    }
  },

  async updateKid(req, res) {
    const kid_id = req.params;
    try {
      await Kids.update(req.body, {
        where: {
          id: parseInt(kid_id),
        },
      });
      return res.status(200).json("Perfil atualizado com sucesso!")
    } catch (error) {
      return res.status(400).json("Falha ao atualizar o perfil!")
    }
  },

  async destroy(req, res) {
    const params = req.params;

    const kid = await Kids.findOne({
      where: {
        id: parseInt(params.id),
      },
    });

    user.destroy();

    res.json("Perfil excluído!");
  },

}