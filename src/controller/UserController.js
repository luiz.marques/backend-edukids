const User = require("../database/models/User");
const Kids = require("../database/models/Kids");
const AuthVideo = require("../database/models/AuthVideo");
const Subscription = require("../database/models/Subscription");
const Plans = require("../database/models/Plan");
const Video = require("../database/models/Video");
const { generateHash } = require("../utils/helper");
const Avatar = require("../database/models/Avatar");
const { Op } = require('sequelize');
const { getParentalString } = require("../services/parentalGuide")

module.exports = {
  async index(req, res) {
    const user = await User.findAll({
      order: [
        ["id", "ASC"]
      ]
    });

    return res.json(user);
  },

  async indexShowKids(req, res) {
    const users_id = req.userId
    // try {
    const kids = await Kids.findAll({
      where: {
        users_id
      },
      include: [
        {
          model: Avatar
        }
      ]
    });
    return res.json(kids);
    // }catch(error){
    //   console.log(JSON.stringify(error))
    //   return res.status(500).json(error)
    // }
  },

  async show(req, res) {
    const { id } = req.params
    const user = await User.findByPk(id, {
      attributes: ["name", "email", "phone", "address"],
    });
    return res.json(user);
  },

  async showSingleAuthVideos(req, res) {
    const kids_id = req.params.id //const { id:kids_id } = req.params
    const users_id = req.userId
    try {
      const authVideos = await AuthVideo.findAll({
        where: {
          users_id,
          kids_id
        },
        attributes: ["id", "users_id", "videos_id"],
        order: [["videos_id", "ASC"]],

        //Para incluir os dados da tabela já vinculada diretamente no BD, basta um include simples do Model (tem que add no require no topo). 
        include: [
          {
            model: Video,
            as: 'videos',
            attributes: ["id", "title", "video_url", "thumb_url", "description", "pg"],
          },
        ],
      });
      return res.json(authVideos)
    } catch (error) {
      console.log(error);
    }
  },

  async showSubscriptionPlan(req, res) {
    // const plans_id = req.params.id
    const users_id = req.userId
    try {
      const subscriptionPlan = await Subscription.findOne({
        where: {
          user_id: users_id
        },
        attributes: ["id", "user_id", "plans_id", "active", "expires_in"],
      });
      console.log('#######################', subscriptionPlan)
      return res.json(subscriptionPlan)
    } catch (error) {
      console.log(error);
    }
  },

  async showAuthVideos(req, res) {
    const user_id = req.userId
    const kids_id = req.params.id
    try {

      const where = {
        users_id: parseInt(user_id),
        videos_id: {
          [Op.not]: null
        }
      }

      if (kids_id) {
        where.kids_id = kids_id
      }
      const authVideos = await AuthVideo.findAll({
        where,
        attributes: ["id", "users_id", "videos_id"],
        order: [["videos_id", "ASC"]],

        //Para incluir os dados da tabela já vinculada diretamente no BD, basta um include simples do Model (tem que add no require no topo). 
        include: [
          {
            model: Video,
            as: 'videos',
            attributes: ["id", "title", "video_url", "thumb_url", "description", "pg"],
          },
        ],
      });

      const videosResponse = authVideos.map(item => {
        return {
          ...item.dataValues,
          parentalGuide: getParentalString(item.dataValues.videos.pg)
        }
      })

      return res.json(videosResponse);
    } catch (error) {
      res.status(500).json(error);
    }
  },

  async store(req, res) {
    const { name, cpf, birthday, email, phone, address, password } = req.body;
    const created_at = new Date()
    const updated_at = new Date()
    const passHash = await generateHash(password)

    try {
      const user = await User.create({
        name,
        cpf,
        birthday,
        email,
        phone,
        address,
        password: passHash,
        user_type: 2,
        created_at,
        updated_at
      });
      return res.status(200).json("Usuario adicionado com sucesso!")
    } catch (error) {
      return res.status(400).json("Falha ao adicionar o usuario.")
    }
  },

  async updateUser(req, res) {
    const { id } = req.params;
    try {
      await User.update(req.body, {
        where: {
          id: parseInt(id),
        },
      });
      return res.status(200).json("Usuário atualizado com sucesso")
    } catch (error) {
      return res.status(400).json("Falha ao atualizar o Usuário")
    }
  },

  async destroy(req, res) {
    const params = req.params;

    const user = await User.findOne({
      where: {
        id: parseInt(params.id),
      },
    });

    user.destroy();

    res.json("Usuario deletado!");
  },
}