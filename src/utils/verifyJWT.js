const jwt = require('jsonwebtoken');
const jwtHashSecret = process.env.SECRET_KEY;


const verifyJWT = function(req, res, next){
  const headerAuth = req.headers.authorization
  console.log('received ', headerAuth)
  if (!headerAuth) return res.status(403).json({ status: false, mensagem: 'Sem chave de acesso.' })
  
  const parts = headerAuth.split(" ");
  if (parts.length !== 2) {
    return res.status(401).json({ error: "Token error" });
  }

  const [schema, token] = parts;

  if (schema.toLowerCase() !== "bearer") {
    return res.status(401).json({ error: "Token malformatted" });
  }

  
  jwt.verify(token, jwtHashSecret, function(err, decoded) {
      if (err) return res.status(401).json({ status: false, mensagem: 'Falha ao autenticar token.' })

      //set user tipo and userid to routes
      req.name = decoded.name
      req.userId = decoded.id
      //console.log('user',req.permission, req.userId)
      // console.log('token identificado', req.permission)
      next();
  });
}

module.exports = {
  verifyJWT
}
