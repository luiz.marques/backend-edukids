const express = require("express");
const routes = express.Router();
const glob = require("glob");
const path = require("path");
const { verifyJWT } = require("./utils/verifyJWT");
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const defaultPath = path.join(__dirname, "routes")

const swaggerOptions = {
  swaggerDefinition: {
      info: {
          title: 'EDUKIDS',
          description: 'EDUKIDS BACKEND API',
          contact: {
              name: 'Infnet Bootcamp'
          },
      },
      components: {
        securitySchemes: {
          jwt: {
            type: "http",
            scheme: "bearer",
            in: "header",
            bearerFormat: "JWT"
          },
        }
      },
      security: [{
        jwt: []
      }],
      servers: [`http://localhost:${process.env.PORT || 3333}`],
  },
  apis: ['./src/routes/**/*.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

routes.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))

//Junta todos os arquivos publics
glob.sync( path.join(defaultPath,'**/*.js') ).forEach( function( file ) {
  let dir = path.basename(path.dirname(file))
  if(dir == 'public'){
      require(path.resolve(file))(routes);
  }
});

//middleware para verificar se usuário esta logado
routes.use(verifyJWT)

//Junta todos os arquivos privates
glob.sync( path.join(defaultPath,'**/*.js') ).forEach( function( file ) {
  let dir = path.basename(path.dirname(file))
  if(dir == 'private'){
      require(path.resolve(file))(routes);
  }
});






module.exports = routes;


