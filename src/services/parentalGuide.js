const getParentalString = (parentalGuide) => {
  switch (parentalGuide) {
    case 1 : 
      return "0 a 2 anos"  
    case 2 : 
      return "2 a 4 anos"  
    case 3 : 
      return "4 a 6 anos"  
    case 4 : 
      return "6 a 8 anos"
    case 5 : 
      return "8+ anos"
    default: 
      return "Livre"
  }
}

module.exports = {
  getParentalString
}