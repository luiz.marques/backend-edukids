# Projeto 03 - Backend - EDUKIDS

Sistema feito como terceiro desafio técnico do Bootcamp de Full Stack do Instituto Infnet.
É um site de vídeos direcionados ao público infantil, com acesso supervisionado por adultos, que definem quais vídeos cada perfil vinculado pode ter acesso. O usuário realiza o próprio cadastro e o de cada um de seus filhos. Existem ambientes separados para cada perfil com permissões correspondentes. Há autenticação por login e senha e geração de Token para controle de navegação.

## APIs e BDs

Este Backend é a própria API do projeto. <br>
O Frontend que a consome está disponível em: <br><br>
https://edukids.vercel.app/ <br>
https://gitlab.com/diego.prutchi/frontend_edukids

O BD escolhido foi o Postgres e a hospedagem do mesmo encontra-se no ElephantSQL.

## Autores

- [Diego Prutchi](https://linkedin.com/in/diegoprutchi)
- [Karine Paiva](https://www.linkedin.com/in/karinepaiva)
- [Luiz Marques](https://www.linkedin.com/in/luiz-marques-a4307648/)

  
## Live Demo
https://api-edukids.herokuapp.com/


## Tecnologias

Backend feito em NodeJS + ExpressJS com o uso das seguintes "libs":
- Express Validator
- BCrypt
- Cors
- Dot Env
- Joi
- JSON Web Token
- Sequelize
- Swagger UI Express
- PG (Postgres Client)
- PG-Hstore

  
## Feedback

Será muito apreciado. Por favor envie para:

diego.prutchi@al.infnet.edu.br <br>
karine.paiva@al.infnet.edu.br <br>
luiz.marques@al.infnet.edu.br

  
## Execute o projeto localmente

Clone o projeto

```bash
  git clone https://gitlab.com/luiz.marques/backend-edukids.git
```

Vá para a pasta do projeto

```bash
  cd <nome_pasta>
```

Instale as dependências

```bash
  npm install
```

Inicie o Servidor

```bash
  npm start
```

  
